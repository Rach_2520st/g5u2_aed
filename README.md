OBTENCIÓN DEL PROGRAMA:

-Para obtener el programa se debe ejecutar el comando "git clone https://gitlab.com/Rach_2520st/g5u2_aed", este comando permite la clonación del repositorio.

-EJECUCIÓN DEL PROGRAMA:

-Para poder ejecutar el programa se ejecuta el comando: "make" y luego se ejecuta el comando ".\Programa"

-ACERCA DE:
EL programa tiene 8 opciones en su menú.

La primera consiste en agregar elementos a un árbol balanceado, el valor es evaluado para verificar que ninguno de los nodos del árbol se repita y que se almacene en un nuevo modo.La opción 2 imprime por terminal los nodos del árbol en posición horizontal.La opción 3 permite determinar si existe o no un elemento ingresado por el usuario. La 4ta opción imprime los tres tipos de ordenamiento en profundidad al árbol: Preorden, Postorden e Inorden. La opción 5 permite la eliminación de algún nodo ingresado, esto es posible analizando si el nodo es de tipo padre u hoja. La 6ta opción permite la modificación de un nodo, en donde primero es eliminado y luego se solicita el ingreso de un nuevo nodo. La septima opción permite la generación de una imagen formato .png a partir de los datos ingresados a través de Graphviz, la última opción permite que el programa se cierre. 

-DETALLES:

El programa solo permite el ingreso de números enteros.

-REQUISITOS:

Sistema operativo Linux
Herramienta de gestion de dependencias make (para Makefile)
Colección de software Graphviz
Visor de imágenes EOG (Eye of GNOME)
Compilador GNU C++ (g++

-AUTOR:

Rachell Scarlett Aravena Martínez.
