#include <fstream>
#include <iostream>
#include "Grafo.h"
using namespace std;

// para usar fork() 
#include <unistd.h>

//crea el archivo .txt de salida y le agrega
//contenido del árbol para generar el grafo (imagen)
 

Grafo::Grafo (Nodo *nodo) {
	
	ofstream fp;

	
		// Se abre/crea el archivo grafo.txt, a partir de este se generará el grafo 
		fp.open ("grafo.txt");
		// Se escribe dentro del archivo txt 
		fp << "digraph G {" << endl;
		// Se establece el color que representarán a los nodos (amarillo) 
		fp << "node [style=filled fillcolor=yellow];" << endl;
		fp << "null [shape=point];" << endl;
    	fp << "null -> " << nodo->dato << " [label=" << nodo->fe << "]" << endl;

		// Llamado a la función recursiva que genera el archivo de texto para creación del grafo 
		recorrerArbol(nodo, fp);

		// Se termina de escribir dentro del archivo txt
		fp << "}" << endl;

		fp.close();

		//se genera el grafo 
		system("dot -Tpng -ografo.png grafo.txt &");

		
			cout << "Abriendo imagen..." << endl;
			system("eog grafo.png &");
		
	
}


//ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado
//es ofstream &nombre_archivo) 

// recorre el árbol en preorden y agrega datos al archivo 
void Grafo::recorrerArbol(Nodo *p, ofstream &fp) {
	string cadena = "\0";

	// Se enlazan los nodos del grafo, para diferenciar entre izquierda y derecha a cada
	//nodo se le entrega un identificador al final, siendo i: izquierda y
	 //d: derecha, esto se cumplirá para los casos en donde los nodos no
	 //apunten a ningún otro (nodos finales) 
	
	if (p != NULL) {
		// Por cada nodo, ya sea por izquierda o derecha, se escribe dentro de la
		 // instancia del archivo 
		
		if (p->izq != NULL) {
			fp <<  p->dato << "->" << p->izq->dato << "[label=" << p->fe << "];" << endl;
		} else {
			cadena = to_string(p->dato) + "i";
			fp <<"\"" << cadena << "\"" <<"[shape=point];" << endl;
			fp << p->dato << "->" <<"\"" << cadena << "\"" << ";" << endl;
		}

		if (p->der != NULL) {
			fp << p->dato << "->" << p->der->dato << "[label=" << p->fe << "];" << endl;
		} else {
			cadena = to_string(p->dato) + "d";
			fp <<"\"" << cadena << "\"" << "[shape=point];" << endl;
			fp << p->dato << "->" <<"\"" << cadena << "\"" << ";" << endl;
		}

		// realiza llamadas a la izquierda y derecha para la creación del grafo 
		recorrerArbol(p->izq, fp);
		recorrerArbol(p->der, fp);
	}
}
