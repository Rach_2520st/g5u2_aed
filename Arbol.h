#include <iostream>
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H


typedef struct _Nodo {
	int dato;
	int fe; //factor de equilibrio
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo;


class Arbol {
	public:
		// Constructor
		Arbol();

		// Metodos 
		Nodo *crearNodo (int);
		void insertarNodo (Nodo *&, int, bool &); //pasar el arbol por ref
		void mostrarArbol (Nodo *, int);
		bool busqueda (Nodo *, int); //busca elemento solicitado
		
		void eliminarNodo (Nodo *&, int, bool &);
		void reestrucDer (Nodo *&, bool &);
		void reestrucIzq (Nodo *&, bool &);
		void graficar (Nodo *); //crea archivo del grafo

		//Ordenamientos
		void preOrden (Nodo *);
		void inOrden (Nodo *);
		void postOrden (Nodo *);

	private:
		Nodo *nodo1 = new Nodo();
		Nodo *nodo2 = new Nodo();

		// Auxiliares para eliminacion
		Nodo *otro = NULL;
		Nodo *aux = NULL;
		Nodo *aux1 = NULL;

};
#endif
