#include <iostream>
#include <fstream>
#include "Arbol.h"
#include "Grafo.h"

Arbol::Arbol(){}

//función que permite crear un nuevo nodo
Nodo* Arbol::crearNodo(int n) {
	Nodo *nuevo_nodo; 

	nuevo_nodo = new Nodo(); //Hace que el sistema reserve memoria
	nuevo_nodo->dato = n; //Asigna valor a nodo
	nuevo_nodo->fe = 0; //Factor de equilibrio
	nuevo_nodo->izq = NULL;
	nuevo_nodo->der = NULL;

	return nuevo_nodo;
}

// función que permite insertar elementos en el arbol 
void Arbol::insertarNodo(Nodo *&nodo, int n, bool &altura){

	if (nodo != NULL) {
		//Obtener el valor de la raiz
		int valorRaiz = nodo->dato;

		// Si el elemento es menor a la raiz, insertar en rama izquierda,
		 // debe indicar si la altura aumenta o dismunuye 
		if (n < valorRaiz) {
			insertarNodo(nodo->izq, n, altura);

			if (altura == true)	{ //si la altura disminuye
				//se analiza el factor de equilibrio para ver si esta balanceado
				
				switch (nodo->fe) {
					//el factor de equilibrio ayuda a establecer la rotacion
					case 1:
						nodo->fe = 0;
						altura = false;
						break;

					case 0:
						nodo->fe = -1;
						break;

					case -1:
						nodo1 = nodo->izq;
						//se reestructura el arbol

						if (nodo1->fe <= 0) {
							// Rotacion Izquierda-Izquierda
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->fe = 0;
							nodo = nodo1;
						}
						else {
							// Rotacion Izquierda-Derecha 
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;

							//si el factor es -1
							if (nodo2->fe == -1) {
								nodo->fe = 1;
							} else {
								nodo->fe = 0;
							}

							//si el factor es 1
							if (nodo2->fe == 1){
								nodo1->fe = -1;
							} else {
								nodo1->fe = 0;
							}
							nodo = nodo2;
						}

						nodo->fe = 0;
						altura = false;
						break;
				}
			}
		}
		//Otros casos pueden ser; que se tenga que añadir al árbol en rama
		 //derecha o no añadir (elemento repetido) 
		else {
			// Si el elemento es mayor a la raiz, insertar en rama derecha
			 
			if (n > valorRaiz) {
				insertarNodo(nodo->der, n, altura);

				if (altura == true) {
					// Analizar el factor de equilibrio para determinar que
					// metodo de rotacion se va a utilizar 
					switch(nodo->fe){
						case -1:
							nodo->fe = 0;
							altura = false;
							break;

						case 0:
							nodo->fe = 1;
							break;

						case 1:
							nodo1 = nodo->der;
							//reestructurar el arbol

							if (nodo1->fe >= 0) {
								// Rotacion Derecha-Derecha para el proceso de balanceo
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->fe = 0;
								nodo = nodo1;
							}
							else {
								// Rotacion Derecha-Izquierda para el proceso de balanceo
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;

								// si el factor es 1 
								if (nodo2->fe == 1) {
									nodo->fe = -1;
								} else {
									nodo->fe = 0;
								}

								// si el factor es -1 
								if (nodo2->fe == -1) {
									nodo1->fe = 1;
								} else {
									nodo1->fe = 0;
								}
								nodo = nodo2;
							}
							nodo->fe = 0;
							altura = false;
							break;
					}//fin switch
				}//fin altura true
			}
			//De lo contrario signifca que el valor que se quiere insertar ya
			//existe. Como no se permite la duplicidad de este dato no se inserta
			else {
				// Elemento repetido 
				cout << "El valor que ingresó" << n << " ya existe en el árbol" << endl;
			}
		}
	}
	//Si el nodo recibido fuera nulo entonces el nuevo nodo se puede insertar
	//en esa posición y se terminan las llamadas recursivas a este método 
	else {
		//se crea un nuevo nodo y  se le pasa el elemento e indica su padre 
		Nodo *nuevo_nodo = crearNodo(n);
		altura = true; //Hay un cambio en la altura (aumenta)
		nodo = nuevo_nodo; //Almacena el dato en la raiz
		cout << " Elemento insertado con exito" << endl;
	}
}

// función para mostrar arbol completo de forma horizontal 
void Arbol::mostrarArbol(Nodo *arbol, int cont){ 


	if (arbol == NULL) {
		// Arbol vacio 
		return;
	} else {
		// Empieza por el lado derecho 
		mostrarArbol (arbol->der, cont+1 );
		for (int i = 0; i < cont; ++i) {
			cout << "          "; 
		}
		cout << arbol->dato << endl;
		//lado izquierdo 
		mostrarArbol(arbol->izq, cont+1 );
	}
}

// función para buscar un elemento en el arbol 
bool Arbol::busqueda(Nodo *arbol, int n){
	if (arbol == NULL) {
		// Arbol vacio 
		return false;
	} else if (arbol->dato == n) {
		//Si el nodo actual es igual al elemento 
		return true;
	} else if (n < arbol->dato) {
		// Si la busqueda es menor al nodo, analiza rama izquierda 
		return busqueda(arbol->izq, n);
	} else {
		// Si la busqueda es mayor al nodo, analiza rama derecha
		return busqueda(arbol->der, n);
	}
}

//función para ordenamiento preorden
void Arbol::preOrden(Nodo *arbol){
	if (arbol == NULL) {
		// Arbol vacio 
		return;
	} else {
		
		cout << arbol->dato << " - ";
		preOrden(arbol->izq);
		preOrden(arbol->der);
	}
}

//función para ordenamiento Inorden
void Arbol::inOrden(Nodo *arbol){
	if (arbol == NULL) {
		// Arbol vacio 
		return;
	} else {
		// Primero el subarbol izquierdo, luego la raiz
		 //y despues el subarbol derecho
		inOrden(arbol->izq);
		cout << arbol->dato << " - ";
		inOrden(arbol->der);
	}
}

//función para recorrido PostOrden
void Arbol::postOrden(Nodo *arbol){
	if (arbol == NULL) {
		// Arbol vacio 
		return;
	} else {
		// Primero el subarbol izquierdo, luego el derecho
		// y despues la raiz 
		postOrden(arbol->izq);
		postOrden(arbol->der);
		cout << arbol->dato << " - ";
	}
}

// función para eliminar un nodo 
void Arbol::eliminarNodo(Nodo *&nodo, int n, bool &altura){
	// bool indica que la altura del arbol ha disminuido, inicia en false
	// Usa 2 algoritmos axiliares: reestrucDer y reestrucIzq,
	// ademas de 3 variables auxiliares de tipo puntero: otro, aux y aux1 
	if (nodo != NULL) {
		// Obtiene el valor de la raiz
		int valorRaiz = nodo->dato;

		// Si el elemento es menor a la raiz, insertar en izquierda
		// e indicar su altura 
		if (n < valorRaiz) {
			eliminarNodo(nodo->izq, n, altura);
			reestrucIzq(nodo, altura);
		}
		else {
			// Si el elemento es mayor a la raiz, insertar en derecho
		 	// e indicar la altura 
			if (n > valorRaiz) {
				eliminarNodo(nodo->der, n, altura);
				reestrucDer(nodo, altura);
			}
			else {
				otro = nodo;
				altura = true; //altura disminuyó

				if (otro->der == NULL) {
					nodo = otro->izq;
				}
				else {
					if (otro->izq == NULL) {
						nodo = otro->der;
					}
					else {
						aux = nodo->izq;
						altura = false;

						while (aux->der != NULL){
							aux1 = aux;
							aux = aux->der;
							altura = true;
						}
						nodo->dato = aux->dato;
						otro = aux;

						if (altura == true) {
							aux1->der = aux->izq;
						}
						else {
							nodo->izq = aux->izq;
						}
						reestrucDer(nodo->izq, altura);
					}
				}
				delete otro; //liberar memoria del nodo
			}
		}
	}
	else {
		cout << "\n Elemento <" << n << "> no existe" << endl;
		cout << " Imposible de eliminar" << endl;
	}
}



void Arbol::reestrucIzq(Nodo *&nodo, bool &altura){
	//bool pasado por ref, indica q la altura de la rama izq ha disminuido
	//Se usa cuando la altura de la rama izquierda ha disminuido y el factor
	 // de equilibrio es 1 
	if (altura == true) { //Altura de la rama izq ha disminuido
		// Analiza el factor de equilibrio para ver si esta balanceado el árbol 
		switch (nodo->fe) {
			case -1:
				nodo->fe = 0;
				break;

			case 0:
				nodo->fe = 1;
				altura = false;
				break;

			case 1:
				// Reestructuración del árbol
				nodo1 = nodo->der;
				//factor mayor o igual a 0
				if (nodo1->fe >= 0)	{
					// Rotación Derecha-Derecha para el proceso de balanceo 
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;

					switch (nodo1->fe){
						case 0:
							nodo->fe = 1;
							nodo1->fe = -1;
							altura = false;
							break;

						case -1:
							nodo->fe = 0;
							nodo1->fe = 0;
							break;
					}
					nodo = nodo1;
					//fin rotacion Derecha-Derecha
				}
				else {
					// Rotación Derecha-Izquierda  para el proceso de balanceo 
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;

					if (nodo2->fe == 1) {
						nodo->fe = -1;
					} else {
						nodo->fe = 0;
					}

					if (nodo2->fe == -1) {
						nodo1->fe = 1;
					} else {
						nodo1->fe = 0;
					}

					nodo = nodo2;
					nodo2->fe = 0;
				//fin rotacion Derecha-Izquierda
				}
				break;
		}
	}
}


void Arbol::reestrucDer(Nodo *&nodo, bool &altura){
	//bool indica q la altura de la rama der ha disminuido
	// Se usa cuando la altura de la rama derecha ha disminuido y el factor de equilibrio es -1 
	if (altura == true) { //Altura disminuyó
		// Analizar el factor de equilibrio para ver si esta balanceado el árbol 
		switch (nodo->fe) {
			case 1:
				nodo->fe = 0;
				break;

			case 0:
				nodo->fe = -1;
				altura = false;
				break;

			case -1:
				// Reestructuración del árbol
				nodo1 = nodo->izq;
				//factor menor o igual a 0
				if (nodo1->fe <= 0)	{
					// Rotación Izquierda-Izquierda para el proceso de balanceo 
					nodo->izq = nodo1->der;
					nodo1->der = nodo;

					switch (nodo1->fe){
						case 0:
							nodo->fe = -1;
							nodo1->fe = 1;
							altura = false;
							break;

						case -1:
							nodo->fe = 0;
							nodo1->fe = 0;
							break;
					}
					nodo = nodo1;
				}
				else {
					// Rotación Izquierdo-Derecha para el proceso de balanceo
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;

					if (nodo2->fe == -1) {
						nodo->fe = 1;
					} else {
						nodo->fe = 0;
					}

					if (nodo2->fe == 1) {
						nodo1->fe = -1;
					} else {
						nodo1->fe = 0;
					}

					nodo = nodo2;
					nodo2->fe = 0;
				}
				break;
		}
	}
}

// función para generar imagen del arbol 
void Arbol::graficar(Nodo *arbol){
	if (arbol == NULL) {
		// Arbol vacio 
		return;
	} else {
		cout << " Iniciando creacion del grafico" << endl;
		Grafo *grap = new Grafo(arbol);
	}
}
