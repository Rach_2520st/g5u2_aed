#include <iostream>
#include <fstream>
#include "Arbol.h"

using namespace std;

#ifndef GRAFO_H
#define GRAFO_H


class Grafo {

	public:
		// constructor
		Grafo(Nodo *nodo);

		// Métodos de la clase Grafo 

		//recorrerArbol: recorre el archivo grafo y escribe en él
		void recorrerArbol (Nodo *, ofstream &);

};
#endif
